FROM alpine AS builder

ADD https://ftp.gnu.org/gnu/aspell/aspell-0.60.8.tar.gz /
RUN apk add build-base perl
RUN tar xzf /aspell-0.60.8.tar.gz
WORKDIR /aspell-0.60.8
RUN ./configure && make && make install

WORKDIR /
ADD https://ftp.gnu.org/gnu/aspell/dict/en/aspell6-en-2019.10.06-0.tar.bz2 /
RUN tar xjf /aspell6-en-2019.10.06-0.tar.bz2
WORKDIR /aspell6-en-2019.10.06-0
RUN ./configure && make && make install

FROM alpine
RUN apk add libstdc++
COPY --from=builder /usr/local /usr/local
